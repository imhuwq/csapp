#include <cstdio>

typedef unsigned char *byte_pointer;

void show_bytes_reversed(byte_pointer p, unsigned int len) {
  int i = len - 1;
  printf("0x");
  while (i >= 0)
    printf("%.2x", p[i--]);
  printf("\n");
}

void show_int_reversed(int num) {
  show_bytes_reversed((byte_pointer) &num, sizeof(int));
}

int bool_or(int x, int y) {

}

int bool_xor(int x, int y) {
  
}

int main() {
  int x = 0x87654321;
  int mask = 0xff;

  show_int_reversed(x & mask);
  show_int_reversed(x ^ ~mask);
  show_int_reversed(x | mask);
}
