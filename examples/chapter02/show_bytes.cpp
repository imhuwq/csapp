#include <cstdio>

typedef unsigned char *byte_pointer;

void show_bytes(byte_pointer p, unsigned int len) {
  int i = 0;
  printf("0x");
  while (i < len)
    printf("%.2x", p[i++]);
  printf("\n");
}

void show_int(int num) {
  show_bytes((byte_pointer) &num, sizeof(int));
}

void show_long(long num) {
  show_bytes((byte_pointer) &num, sizeof(long));
}

void show_float(float num) {
  show_bytes((byte_pointer) &num, sizeof(float));
}

void show_double(double num) {
  show_bytes((byte_pointer) &num, sizeof(double));
}

void show_pointer(void *pointer) {
  show_bytes((byte_pointer) &pointer, sizeof(void *));
}

int main(const int argc, const char **argv) {
  int numInt = 1234567;
  long numLng = 1234567L;
  float numFlt = 1234567.0F;
  double numDbl = 1234567.0;
  int *numIntPtr = &numInt;

  printf("size of char: %i\n", sizeof(char));
  printf("size of short: %i\n", sizeof(short));
  printf("size of int: %i\n", sizeof(int));
  printf("size of long: %i\n", sizeof(long));
  printf("size of float: %i\n", sizeof(float));
  printf("size of double: %i\n", sizeof(double));
  printf("size of pointer: %i\n", sizeof(void*));

  show_int(numInt);
  show_long(numLng);
  show_float(numFlt);
  show_double(numDbl);
  show_pointer(numIntPtr);

  show_int(0xabcd1234);
  show_int(0xffffffff);
}